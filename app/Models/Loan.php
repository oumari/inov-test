<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    use HasFactory;
	protected $fillable = [
        'id',
        'client_name',
        'user_id',
        'loan_amount',
		'start_date',
		'end_date',
		'bank_margin',	
		'repaid_amount'
		];
}

