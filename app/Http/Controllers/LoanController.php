<?php

namespace App\Http\Controllers;

use App\Models\Loan;
use Illuminate\Http\Request;
use Response;
use Auth;
class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	    public function __construct()
    {
        $this->middleware('auth');
    }

	public function track_repaid($loans)
	{
		#not started means that payments will be paid in future
		#if the user deposit a repaid amount we have to show him the rest of the payments as pourcentage % 
	  $data_array=array();
      foreach($loans as $loan)
		 { 
		 if($loan->start_date<=date("Y-m-d")and $loan->end_date>date("Y-m-d"))
		 {
			 $data_array{"data"}[] = array(
			   'id'=>$loan->id,
				'client_name'=>$loan->client_name,
				'loan_amount'=>$loan->loan_amount,
				'start_date'=>$loan->start_date,
				'end_date'=>$loan->end_date,
				'bank_margin'=>$loan->bank_margin,
				'repaid_amount'=>$loan->repaid_amount,
				'repaid_status'=>['active',number_format(($loan->repaid_amount/($loan->loan_amount*(1+$loan->bank_margin)))*100 , 2, '.', ','). " %"],

				);
		 }
		 elseif($loan->start_date>date("Y-m-d") )
         {
			  $data_array{"data"}[] = array(
			    'id'=>$loan->id,
				'client_name'=>$loan->client_name,
				'loan_amount'=>$loan->loan_amount,
				'start_date'=>$loan->start_date,
				'end_date'=>$loan->end_date,
				'bank_margin'=>$loan->bank_margin,
				'repaid_amount'=>$loan->repaid_amount,
				'repaid_status'=>['Not started',number_format(($loan->repaid_amount/($loan->loan_amount*(1+$loan->bank_margin)))*100 , 2, '.', ','). " %"],
				);
		 }
		elseif(number_format(($loan->repaid_amount/($loan->loan_amount*(1+$loan->bank_margin)))*100)<100 and $loan->end_date<date("Y-m-d") )
		{
			 $data_array{"data"}[] = array(
			    'id'=>$loan->id,
				'client_name'=>$loan->client_name,
				'loan_amount'=>$loan->loan_amount,
				'start_date'=>$loan->start_date,
				'end_date'=>$loan->end_date,
				'bank_margin'=>$loan->bank_margin,
				'repaid_amount'=>$loan->repaid_amount,
				'repaid_status'=>['Expired',number_format(($loan->repaid_amount/($loan->loan_amount*(1+$loan->bank_margin)))*100 , 2, '.', ','). " %"],
				);
		}
        elseif(number_format(($loan->repaid_amount/($loan->loan_amount*(1+$loan->bank_margin)))*100)==100)
		{
			 $data_array{"data"}[] = array(
			    'id'=>$loan->id,
				'client_name'=>$loan->client_name,
				'loan_amount'=>$loan->loan_amount,
				'start_date'=>$loan->start_date,
				'end_date'=>$loan->end_date,
				'bank_margin'=>$loan->bank_margin,
				'repaid_amount'=>$loan->repaid_amount,
				'repaid_status'=>['paid',number_format(($loan->repaid_amount/($loan->loan_amount*(1+$loan->bank_margin)))*100 , 0, '.', ','). " %"],
				);
		}else
		{
			 $data_array{"data"}[] = array(
				'id'=>$loan->id,
				'client_name'=>$loan->client_name,
				'loan_amount'=>$loan->loan_amount,
				'start_date'=>$loan->start_date,
				'end_date'=>$loan->end_date,
				'bank_margin'=>$loan->bank_margin,
				'repaid_amount'=>$loan->repaid_amount,
				'repaid_status'=>['Error','Error'],
				);
		}
		 }
		 if($data_array)
		 {
		    $loans=json_decode(json_encode($data_array['data'], true));
		return $loans;
		 }else{
			 return $loans;
		 }
		 
	}
	
    public function index()
    {   $user_id=Auth::user()->id;
     	$loans = Loan::All()->sortBy("created_at")->where('user_id',$user_id);
        $loans= $this->track_repaid($loans);
        return view('home',compact('loans'));
    }
    // public function data()
    // {
        // $loans= $this->track_repaid($loans);
	
        // return $loans;
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $loan_id = $request->loan_id;
		 $user_id=Auth::user()->id;
		 // dd($user_id);
         $loan   =   Loan::updateOrCreate(['id' => $loan_id,'user_id' => $user_id,],
               ['user_id' => $user_id,'client_name' => $request->client_name, 'loan_amount' => $request->loan_amount,
			   'start_date' => $request->start_date, 'end_date' => $request->end_date, 'repaid_amount' => $request->repaid_amount,
			   'bank_margin' => $request->bank_margin,
			   ]);
    
        return Response::json($loan);
        // return Response::json(['response')=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function show(Loan $loan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function edit(Loan $loan)
    {
       // $id = array('id' => $id);
	   $id=$loan->id;
	   // dd($id);
        $loan  = Loan::where('id',$id)->first();
 
        return Response::json($loan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loan $loan)
    {
       
         // $loan_id = $request->id;
		 // dd($request);
         // $loan   =   Loan::update(['id' => $loan_id],
               // ['client_name' => $request->client_name, 'loan_amount' => $request->loan_amount,
			   // 'start_date' => $request->start_date, 'end_date' => $request->end_date,
			   // 'bank_margin' => $request->bank_margin,
			   // ]);
    
        return Response::json($loan);
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan)
    {
        $loan = Loan::where('id',$id)->delete();
   
        return Response::json($loan);
    }
}
