@extends('layouts.app')
@section('content')
 <style>
   
td {
 white-space: nowrap;
}
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header ">{{ __('Loan Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   <!--start-->
				   <div class="container">
    <div class="row">
        <div class="col-12">
          <a href="javascript:void(0)" class="btn btn-info mb-2 float-right" id="create-new-loan">Create Loan</a> 
		    <div class="table-responsive table-bordered"> 
            <table class="table" id="data-table" width="80%" cellspacing="0">
           <thead>
              <tr>
                 <th>Id</th>
                 <th>Client Name</th>
                 <th>Loan Amount</th>
                 <th>Bank Mangin</th>
                 <th>Start date</th>
                 <th>End date</th>
                 <th>Repaid Amount</th>
                 <th>Repaid status</th>
                 <td>Action</td>
              </tr>
           </thead>
           <tbody id="loans-crud">
              @foreach($loans as $loan)
              <tr id="loan_id_{{ $loan->id }}">
                 <td>{{ $loan->id  }}</td>
                 <td>{{ $loan->client_name }}</td>
                 <td>{{ $loan->loan_amount }}</td>
                 <td>{{ $loan->bank_margin }}</td>
                 <td>{{ $loan->start_date }}</td>
                 <td>{{ $loan->end_date }}</td>
				 <td>{{ $loan->repaid_amount}}</td>
                 @if($loan->repaid_status[0]=="active")
				  <td style='color:blue'>{{ $loan->repaid_status[1] }}</td>
				 @elseif($loan->repaid_status[0]=="Expired")
				  <td style='color:red'>{{ $loan->repaid_status[1] }}</td>
				 @elseif($loan->repaid_status[0]=="Not started")
				  <td style='color:grey'>Not Started </td>
				  @elseif($loan->repaid_status[0]=="paid")
				  <td style='color:Green'>{{ $loan->repaid_status[1] }} (Paid) </td>
				 @endif
					    
					 </td>
               
                 <td><a href="javascript:void(0)" id="edit-loan" data-id="{{ $loan->id }}" class="btn btn-info">Update</a></td>
                 <!--td>
                  <a href="javascript:void(0)" id="delete-loan" data-id="{{ $loan->id }}" class="btn btn-danger delete-loan">Delete</a></td-->
              </tr>
              @endforeach
           </tbody>
          </table>
          
       </div> 
       </div> 
    </div>
</div>
<div class="modal fade" id="loan-crud-modal" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="loanCrudModal"></h4>
    </div>
    <div class="modal-body">
        <form id="loanForm" name="loanForm" class="form-horizontal">
           <input type="hidden" name="loan_id" id="loan_id">
            <div class="form-group row">
                <div class="col-sm-6">
					<label for="client_name">Client Name</label><br>
                    <input type="text" class="form-control" id="client_name" name="client_name" value="" placeholder="Client Name" required="">
                </div>
				  <div class="col-sm-6">
				    <label for="start_date">Loan Amount</label><br>
                    <input type="number" class="form-control" id="loan_amount" name="loan_amount" min="10" value="" placeholder="Loan Amount" required="">
                </div>
				 </div>
			<div class="form-group row">
			              <div class="col-sm-6">
						   <label for="start_date">Start date</label><br>
                    <input type="date" class="form-control" id="start_date" name="start_date" value="<?php echo date('Y-m-d'); ?>" min="<?php echo date('Y-m-d'); ?>" placeholder="Current date" >
                </div>
				  <div class="col-sm-6">
				  <label for="start_date">End date</label><br>
                    <input type="date" class="form-control" id="end_date" name="end_date" value="<?php echo date('Y-m-d'); ?>" placeholder="End date" required="">
                </div>
				 </div>
 
            <div class="form-group row">
                <div class="col-sm-6 repaid_amount">
				 <label for="start_date">Repaid Amount</label><br>
                    <input class="form-control repaid_amount" id="repaid_amount" name="repaid_amount" max="" value="0" placeholder="200.00 Euros" >
                </div>
			<div class="col-sm-6">
				 <label for="start_date">Bank Margin</label><br>
                    <input type="number" class="form-control" id="bank_margin" min="0.1" name="bank_margin" value="0.50" placeholder="Bank Margin">
                </div>
            </div>
            <div class="col-sm-offset-2 col-sm-10">
             <button type="submit" class="btn btn-primary" id="btn-save" value="create">Validate
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
$("#end_date").change(function () {
    var start_date = document.getElementById("start_date").value;
    var end_date = document.getElementById("end_date").value;

    if ((Date.parse(start_date) >= Date.parse(end_date))) {
        alert("End date must be greater than Start date");
        document.getElementById("end_date").value = "";
    }
});
</script>
<script>

  $(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#create-new-loan').click(function () {
        $('#btn-save').val("create-loan");
        $('#loanForm').trigger("reset");
        $('#loanCrudModal').html("Create New Loan");
        $('#loan-crud-modal').modal('show');
        $('.repaid_amount').hide();
		
    });
 
    $('body').on('click', '#edit-loan', function () {
      var loan_id = $(this).data('id');
      $.get('loans/'+loan_id+'/edit', function (data) {
         $('#loanCrudModal').html("Edit Loan");
          $('#btn-save').val("edit-loan");
          $('#loan-crud-modal').modal('show');
          $('#loan_id').val(data.id);
          $('#client_name').val(data.client_name);
          $('#loan_amount').val(data.loan_amount);
          $('#start_date').val(data.start_date);  
          $('#end_date').val(data.end_date);  
          $('#repaid_amount').val(data.repaid_amount); 
		  //make a validation that determines max_repaid_value to avoid bypassing due amount
		  // also on update you can only increase repaid amount deceasing it is forbidden
		  var loan_amount=data.loan_amount
		  var repaid_amount=data.repaid_amount; 
		  var bank_margin=data.bank_margin 
          var max_repaid_value=(loan_amount*(1+bank_margin));		  
		  $("#repaid_amount").attr({"max" : max_repaid_value,"min" : repaid_amount});
		
      })
   });
   
    $('body').on('click', '.delete-loan', function () {
        var loan_id = $(this).data("id");
        confirm("Are You sure want to delete !");
        $.ajax({
            type: "DELETE",
            url: "{{ url('loans')}}"+'/'+loan_id,
            success: function (data) {
                $("#loan_id_" + loan_id).remove();
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
    });   
  });
 
 if ($("#loanForm").length > 0) {
      $("#loanForm").validate({
 
     submitHandler: function(form) {
      var actionType = $('#btn-save').val();
      $('#btn-save').html('saving..');
      
      $.ajax({
          data: $('#loanForm').serialize(),
          url: "{{ route('loans.store') }}",
          type: "POST",
          dataType: 'json',
          success: function (data) {
			  location.reload(true); 
              var loan = '<tr id="loan_id_' + data.id + '"><td>' + data.id + '</td><td>' + data.client_name + '</td><td>' + data.loan_amount + '</td><td>' + data.bank_margin + '</td><td>' + data.start_date + '</td><td>' + data.end_date + '</td><td>' + data.repaid_amount + '</td><td><div class="spinner-border text-danger" role="status"></td>';
              loan += '<td><a href="javascript:void(0)" id="edit-loan" data-id="' + data.id + '" class="btn btn-info">Edit</a></td></tr>';
              // loan += '<td><a href="javascript:void(0)" id="delete-loan" data-id="' + data.id + '" class="btn btn-danger delete-loan">Delete</a></td></tr>';
               
              
              if (actionType == "create-loan") {
                  $('#loans-crud').prepend(loan);
              } else {
                  $("#loan_id_" + data.id).replaceWith(loan);
              }
 
              $('#laonForm').trigger("reset");
              $('#loan-crud-modal').modal('hide');
              $('#btn-save').html('Save Changes');
              
          },
          error: function (data) {
              console.log('Error:', data);
              $('#btn-save').html('Save Changes');
          }
      });
    }
  })
}
   
  
</script>
	<script>
$(document).ready(function() {
     $('#data-table').DataTable({
        "paging": true,
        "ordering": true,
        "info": true,
        "responsive": true,
         orderCellsTop: true,
         } );
         
     $('#data-tabl').DataTable({
        "paging": true,
        "ordering": true,
        "info": true,
        "responsive": true,
         orderCellsTop: true,
         } );
         
          $('#data-tab').DataTable({
        "paging": true,
        "ordering": true,
        "info": true,
        "responsive": true,
         orderCellsTop: true,
         } );
} );
    
         </script>
		 <script src="https://cdn.datatables.net/1.11.4/js/jquery.dataTables.min.js"> </script>
@endsection
